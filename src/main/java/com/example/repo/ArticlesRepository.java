package com.example.repo;
 
import org.springframework.data.jpa.repository.JpaRepository; 
import org.springframework.stereotype.Repository;
import com.example.entities.Articles;
  
@Repository
public interface ArticlesRepository extends JpaRepository<Articles, Integer> {

}
 