package com.example.repo;
 
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.entities.Items;
  
@Repository
public interface ItemsRepository extends JpaRepository<Items, Integer> {
	
}
 