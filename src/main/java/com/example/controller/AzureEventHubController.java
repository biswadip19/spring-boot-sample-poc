package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.service.AzureEventhubService;

@RestController
public class AzureEventHubController {
	
	@Autowired
	AzureEventhubService azureEventHubService;

	@GetMapping("/invokerAzureEventHub") 
	public String getCombinedItemList(){ 
		return String.valueOf(azureEventHubService.invokeEventHub());
	}

}
