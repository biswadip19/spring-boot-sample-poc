package com.example.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.entities.Articles;
import com.example.entities.Items;
import com.example.models.CommonData;
import com.example.repo.ArticlesRepository;
import com.example.repo.ItemsRepository;
import com.example.service.ItemServiceImpl;

@RestController
public class ArticlesController {
	
	private static final Logger LOGGER =  LoggerFactory.getLogger(ArticlesController.class);

	@Autowired
	ArticlesRepository articlesRepository;
	
	@Autowired
	ItemsRepository itemRepo;
	
	@Autowired
	ItemServiceImpl itemServiceImpl;
	
	/*
	 * @Autowired DroolsExecutionService droolsService;
	 */
	/*
	 * @Autowired ArticlesDAO articlesDAO;
	 */
	@GetMapping("/articles") 
	public java.util.Set<CommonData> getAllArticles(){
		LOGGER.info("getAllArticles Method invoked started in the controller..");
		List<Articles> articles = articlesRepository.findAll();
		HashSet<CommonData> commonDatas =  new HashSet<>();
		articles.forEach(data -> 
			commonDatas.add(new CommonData( String.valueOf(data.getId()),data.getTitle(),data.getBody()))
		);
		LOGGER.info("getAllArticles Method invoked ended in the controller..");
		return commonDatas;
	}
	
	@GetMapping("/items") 
	public List<Items> getAllItems(){ 
		LOGGER.info("getAllItems Method invoked started in the controller..");
		System.out.println(itemRepo.findAll());
		LOGGER.info("getAllItems Method invoked started in the controller..");
		return new ArrayList<>();
	}
	
	
	@GetMapping("/getCombinedItemList") 
	public List<CommonData> getCombinedItemList(){ 
		Thread thread = Thread.currentThread();
		LOGGER.info("Call received getCombinedItemList with Thread ID :" +  thread.getId() + " name : " + thread.getName());
		return itemServiceImpl.getCombinedItemDetails();
	}
	
	/*
	 * @GetMapping("/api/v1/drools/rule") public int executeRule(){ return
	 * droolsService.getDiscount(); }
	 */
	
	/*
	 * @GetMapping("/jdbc_articles") public List<Articles> getAllArticlesByJDBC() {
	 * return articlesDAO.getAllArticles(); }
	 */
}
