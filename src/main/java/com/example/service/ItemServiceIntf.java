package com.example.service;

import java.util.List;

import com.example.models.CommonData;

public interface ItemServiceIntf {
	List<CommonData> getCombinedItemDetails();
}
