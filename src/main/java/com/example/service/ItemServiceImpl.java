package com.example.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;

import com.example.models.CommonData;

@Service
public class ItemServiceImpl implements ItemServiceIntf{
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<CommonData> getCombinedItemDetails() {
		List<CommonData> dataList = new ArrayList<>();
		List<Object[]> resultList = entityManager.createQuery("SELECT items.id,items.name,articles.title FROM Items items INNER JOIN items.articles articles where articles.id = '1'").getResultList();
		resultList.forEach( data -> 
			dataList.add(new CommonData(String.valueOf(data[0]),String.valueOf(data[1]),String.valueOf(data[2]))));
		return dataList;
	}
	

}
