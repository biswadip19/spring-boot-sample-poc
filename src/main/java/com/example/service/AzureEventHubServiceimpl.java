/**
 * 
 */
package com.example.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.azure.messaging.eventhubs.EventData;
import com.azure.messaging.eventhubs.EventDataBatch;
import com.azure.messaging.eventhubs.EventHubProducerClient;

/**
 * @author t_biswadip.m
 *
 */
@Service
public class AzureEventHubServiceimpl implements AzureEventhubService {

	private static final Logger log = LoggerFactory.getLogger(AzureEventHubServiceimpl.class);

	@Autowired
	@Qualifier("eventHubProducerClient1")
	EventHubProducerClient producer1;
	
	@Autowired
	@Qualifier("eventHubProducerClient2")
	EventHubProducerClient producer2;
	
	@Override
	public boolean invokeEventHub() {
		boolean status = true;
		try {
			String eventData = "{'eventType':'bill_reminder','customerHash':'d3aa00c65a78ea4d396b909677310ec9','version':'1.0','eventDetails':[{'title':'You have some bills due','billerAccountId':'BPAN000000000000124399','description':'You have some bills pending in your cart','cta':[{'ctaType':'pay_now','ctaText':'Pay Now','conversationText':'Pay Now','style':'selected','ctaUrlPWA':'https://dev-r2.tatadigital.com/paybill','ctaUrlAndroid':'https://dev-r2.tatadigital.com/paybill','ctaUrliOS':'https://dev-r2.tatadigital.com/paybill'}]}]}{'eventType':'bill_reminder','customerHash':'d3aa00c65a78ea4d396b909677310ec9','version':'1.0','eventDetails':[{'title':'You have some bills due','billerAccountId':'BPAN000000000000124399','description':'You have some bills pending in your cart','cta':[{'ctaType':'pay_now','ctaText':'Pay Now','conversationText':'Pay Now','style':'selected','ctaUrlPWA':'https://dev-r2.tatadigital.com/paybill','ctaUrlAndroid':'https://dev-r2.tatadigital.com/paybill','ctaUrliOS':'https://dev-r2.tatadigital.com/paybill'}]}]}";
			EventDataBatch eventDataBatch = producer1.createBatch();
			eventDataBatch.tryAdd(new EventData(eventData));
			producer1.send(eventDataBatch);
			//producer.close();
			log.info("event json payload pushed!!");
		}
		catch (Exception e) {
			status = false;
			log.error("event json payload pushed!!",e.getStackTrace());
		}
		return status;

	}

}
