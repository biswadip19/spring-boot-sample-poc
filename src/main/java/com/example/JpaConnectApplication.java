package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan(basePackages = "com.example.entities")
@EnableJpaRepositories("com.example.repo")
@ComponentScan(basePackages = {"com.example.controller,com.example.service,com.example.config"})
//@ComponentScan(basePackages = {"com.example"})
@SpringBootApplication
public class JpaConnectApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpaConnectApplication.class, args);
	}

}
