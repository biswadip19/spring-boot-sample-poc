package com.example.jdbc.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@ComponentScan("com.example.jdbc.config")
public class SpringJdbcConfig {
	
	@Bean
    public DataSource mysqlDataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://psg-tddev-de-ci-bpy-02.postgres.database.azure.com:5432/TDLBillPay");
        dataSource.setUsername("appuser@psg-tddev-de-ci-bpy-02");
        dataSource.setPassword("TDLBpay@123!");
        return dataSource;
    }

}
