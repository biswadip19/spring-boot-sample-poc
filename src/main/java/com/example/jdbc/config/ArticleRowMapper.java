package com.example.jdbc.config;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.entities.Articles;

public class ArticleRowMapper implements RowMapper<Articles>{

	@Override
	public Articles mapRow(ResultSet rs, int rowNum) throws SQLException {
		final Articles article = new Articles();
		article.setId(rs.getInt("ID"));
		article.setTitle(rs.getString("TITLE"));
		article.setBody(rs.getString("BODY"));
        return article;
	}
	
	

}
