package com.example.jdbc.config;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.example.entities.Articles;

@Repository
public class ArticlesDAO {
	
	private JdbcTemplate jdbcTemplate;
	 
	@Autowired
	public void setDataSource(final DataSource dataSource) {
       jdbcTemplate = new JdbcTemplate(dataSource);
    }
	 
	public List<Articles> getAllArticles() {
        return jdbcTemplate.query("SELECT * FROM ARTICLES where id='1'", new ArticleRowMapper());
	}
	

}
